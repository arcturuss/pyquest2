from abc import ABC, abstractmethod
from enum import Enum
from itertools import cycle
from pathlib import Path
from queue import Queue
from random import choice
import json
import re
from time import sleep, time
import vlc
from config import (
    AUDIO_ROOT,
    SOUND_DEVICES,
    SPECIAL_FILES,
    DEFAULT_VOLUME,
    PLAYER_CONF_FILE,
)


def ensure_audio_dirs():
    if not AUDIO_ROOT.exists():
        AUDIO_ROOT.mkdir()

    for subdir in SOUND_DEVICES.keys():
        absolute_subdir = Path(AUDIO_ROOT.joinpath(subdir))
        if not absolute_subdir.exists():
            absolute_subdir.mkdir()


VLC_INSTANCE = vlc.Instance()


def parse_pa_devices():
    # pa_config = open("c:\\Users\\arcturus\\Downloads\\Telegram Desktop\\default.pa")
    pa_config = open("/etc/pulse/default.pa")
    lines = pa_config.readlines()
    devices = list()
    for line in lines:
        line = line.strip()
        if line.startswith("load-module module-remap-sink"):
            devices.extend(re.findall(r"sink_name=([\w-]*) .*", line))
    return devices


def init_audio_manager():
    audio_manager = AudioManager()
    # files = [f for f in AUDIO_ROOT.iterdir() if f.is_file()]
    # for f in files:
    #     audio_manager.add_audio_file(f)
    return audio_manager


def represents_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


class AudioManager:
    def __init__(self):
        self.__files = dict()
        self.__players = dict()
        self.__devices = parse_pa_devices()
        self.__config = None
        self.apply_config()

    @property
    def files(self):
        return self.__files

    @property
    def players(self):
        return self.__players

    @property
    def devices(self):
        return self.__devices

    def apply_config(self):
        try:
            with open(PLAYER_CONF_FILE) as infile:
                self.__config = json.load(infile)
        except Exception as e:
            print("Error parsing json: check player config file")
            print(e)
            return -1
        players = self.__config
        for player in players:
            default_device = player["default_device"]
            name = player["name"]
            player_type = player["player_type"]
            # UNUSED OPTIONS:
            mode = player["mode"]
            default_volume = player.get("default_volume", DEFAULT_VOLUME)
            crossfade = player["crossfade"]
            crossfade_time = player["crossfade_time"]

            player_name = self.add_player(
                default_device,
                name,
                player_type,
                default_volume,
                crossfade,
                crossfade_time,
            )
            _player = self.__players[player_name]
            audiofiles = player["audiofiles"]
            for audiofile in audiofiles:
                path = audiofile["path"]
                # UNUSED OPTIONS:
                volume = audiofile.get("volume", default_volume)
                device = audiofile.get("device", default_device)
                audio_obj = self.add_audio_file(path, player_type, volume, device)
                _player.add_audio_file(audio_obj)

    def dump_config(self):
        for k, v in self.players.items():
            for item in self.__config:
                if item["name"] != k:
                    continue
                if isinstance(v, SimplePlayer):
                    item["default_volume"] = v.volume
                else:
                    for idx, val in enumerate(item["audiofiles"]):
                        val["volume"] = v.file_volume(idx)
        with open(PLAYER_CONF_FILE, "w") as outfile:
            json.dump(self.__config, outfile, indent=2)

    def add_audio_file(self, path, player_type, volume, device=None):
        path = AUDIO_ROOT.joinpath(path)
        if not Path(path).exists():
            return  # TODO return some sort of error
        if player_type == "simplePlayer":
            audio_file = AudioFile(path, volume)
        else:
            audio_file = BgAudioFile(path, volume, device)
        self.__files[audio_file.file_name] = audio_file
        return audio_file

    def remove_audio_file(self, name):
        # TODO Block if file is used in any player and send their id's
        Path(self.files[name].absolute_path).unlink()
        del self.__files[name]

    def add_player(
        self,
        device,
        name=None,
        player_type=None,
        default_volume=None,
        crossfade=None,
        crossfade_time=None,
        mode=None,
    ):
        device = device.encode()
        if not name:
            names = [
                int(pname) for pname in self.__players.keys() if represents_int(pname)
            ]
            if not names:
                name = str(1)
            else:
                name = str(max(names) + 1)
        if name in self.__players:
            print("player %s exists" % name)
            return  # TODO Add some error message on player name clash
        player_map = {
            "simplePlayer": SimplePlayer,
            "backgroundMusic": BackgroundPlayer,
            "linePlayer": LinePlayer,
        }
        PlayerClass = player_map.get(player_type, SimplePlayer)
        print("Player of type [%s] created" % player_type)
        if not default_volume:
            default_volume = DEFAULT_VOLUME
        self.__players[name] = PlayerClass(
            device, default_volume, mode, crossfade, crossfade_time
        )
        return name

    def test_call(self):
        for _, val in self.__players.items():
            val.test_call()


class AudioFile:
    def __init__(self, path, volume=None):
        self.__file_path = path
        self.__vlc_file = VLC_INSTANCE.media_new(str(path))
        self.__volume = volume or DEFAULT_VOLUME

    @property
    def vlc_file(self):
        return self.__vlc_file

    @property
    def file_name(self):
        return self.file_path.stem

    @property
    def location(self):
        return self.file_path.parent.name

    @property
    def file_path(self):
        return self.__file_path

    @property
    def absolute_path(self):
        return AUDIO_ROOT.joinpath(self.file_path)

    @property
    def volume(self):
        return self.__volume

    @volume.setter
    def volume(self, volume):
        self.__volume = volume
        if self.__volume > 100:
            self.__volume = 100

        if self.__volume < 0:
            self.__volume = 0


class BgAudioFile(AudioFile):
    def __init__(self, path, volume=None, device=None):
        super().__init__(path, volume=volume)
        self.device = device


class AudioPlayer(ABC):
    @property
    @abstractmethod
    def vlc_current_player(self):
        pass

    @property
    def player_status(self):
        return self.vlc_current_player.get_state()

    @property
    def muted(self):
        return self.vlc_current_player.get_media_player().audio_get_mute()

    @property
    @abstractmethod
    def volume(self):
        pass

    @volume.setter
    @abstractmethod
    def volume(self):
        pass

    @property
    @abstractmethod
    def current_device(self):
        pass

    @abstractmethod
    def test_call(self):
        pass

    @property
    def looping(self):
        return self.__looping

    def play(self):
        if self.player_status != vlc.State.Playing:
            self.vlc_current_player.stop()
        self.vlc_current_player.play()
        sleep(0.01)
        self.set_output(self.current_device)
        

    def stop(self):
        self.vlc_current_player.pause()
        sleep(0.1)
        self.vlc_current_player.stop()

    def toggle_mute(self):
        if self.muted:
            self.vlc_current_player.get_media_player().audio_set_mute(False)
        else:
            self.vlc_current_player.get_media_player().audio_set_mute(True)
        return self.muted

    def played_callback(self, event):
        self.set_output(self.current_device)

    def set_output(self, device):
        self.vlc_current_player.get_media_player().audio_output_device_set(None, device)
        # print(device)
        # pass

    @abstractmethod
    def add_audio_file(self, audio_file):
        pass


class SimplePlayer(AudioPlayer):
    def __init__(
        self,
        device,
        default_volume=DEFAULT_VOLUME,
        mode="play_repeat",
        crossfade=False,
        crossfade_time=3,
    ):
        self.__device = device
        self.__vlc_player = VLC_INSTANCE.media_list_player_new()
        self.__vlc_playlist = None
        self.__audio_file = None
        self.__volume = default_volume
        self.__events = self.__vlc_player.event_manager()
        self.__events.event_attach(
            vlc.EventType.MediaListPlayerPlayed, self.played_callback
        )

    @property
    def vlc_current_player(self):
        return self.__vlc_player

    @property
    def audio_file(self):
        return self.__audio_file

    @property
    def current_device(self):
        return self.__device

    @property
    def device(self):
        return self.__device.decode()

    @property
    def volume(self):
        if self.audio_file:
            return self.audio_file.volume
        return self.__volume

    @volume.setter
    def volume(self, volume):
        if self.audio_file:
            self.audio_file.volume = volume
            self.__volume = self.audio_file.volume
        self.vlc_current_player.get_media_player().audio_set_volume(self.volume)

    def add_audio_file(self, audio_file):
        if self.__audio_file:
            self.stop()
        if self.__vlc_playlist is None:
            self.__vlc_playlist = VLC_INSTANCE.media_list_new()
            self.__vlc_player.set_media_list(self.__vlc_playlist)
        self.__audio_file = audio_file
        self.__vlc_playlist.lock()
        self.__vlc_playlist.remove_index(0)
        self.__vlc_playlist.add_media(audio_file.vlc_file)
        self.__vlc_playlist.unlock()
        self.volume = self.__audio_file.volume  # not working?

    def incr_volume(self, player=None):
        self.volume += 5
        return self.volume

    def decr_volume(self):
        self.volume -= 5
        return self.volume

    def test_call(self):
        self.play()
        sleep(0.1)
        self.stop()


class BackgroundPlayer(AudioPlayer):
    def __init__(
        self,
        device,
        default_volume=DEFAULT_VOLUME,
        mode="play_repeat",
        crossfade=False,
        crossfade_time=3,
    ):
        self.__default_device = device
        self.__current_device = self.__default_device
        self.__vlc_player = VLC_INSTANCE.media_list_player_new()
        self.__vlc_player_crossfade = VLC_INSTANCE.media_list_player_new()
        self.__current_index = 0
        self.__vlc_playlist = None
        self.__audio_files = None
        self.__play_next = False
        self.__volume = default_volume
        self.__default_volume = default_volume
        self.__crossfade = crossfade
        self.__crossfade_time = crossfade_time

        self.__playlist_events = self.__vlc_player.get_media_player().event_manager()
        self.__playlist_events.event_attach(
            vlc.EventType.MediaPlayerPlaying, self.__set_volume_callback
        )
        self.__playlist_events_2 = (
            self.__vlc_player_crossfade.get_media_player().event_manager()
        )
        self.__playlist_events_2.event_attach(
            vlc.EventType.MediaPlayerPlaying, self.__set_volume_callback
        )

    @property
    def vlc_current_index(self):
        return self.__current_index

    @property
    def vlc_current_player(self):
        return self.__vlc_player

    @property
    def current_device(self):
        return self.__current_device

    @property
    def file_names(self):
        if self.__audio_files is None:
            return ["--empty--"]
        else:
            return [audio_file.file_name for audio_file in self.__audio_files]

    @property
    def audio_files(self):
        return self.__audio_files

    def __set_volume_callback(self, event):
        self.__vlc_player.get_media_player().audio_set_volume(self.__volume)

    def add_audio_file(self, audio_file, volume=None, device=None):
        # NOTE: audio_file is of type BgAudioFile
        if self.__vlc_playlist is None:
            self.__vlc_playlist = VLC_INSTANCE.media_list_new()
            self.__vlc_player.set_media_list(self.__vlc_playlist)
            self.__vlc_player_crossfade.set_media_list(self.__vlc_playlist)
            self.__audio_files = list()
        if audio_file in self.__audio_files:
            print("File %s not added: already in playlist" % audio_file.file_name)
            return  # TODO Handle existing file error
        self.__vlc_playlist.add_media(audio_file.vlc_file)
        self.__audio_files.append(audio_file)

    def remove_audio_file(self, audio_file):
        if not self.__audio_files:
            return
        index = self.__vlc_playlist.index_of_item(audio_file.vlc_file)
        if index != -1:
            self.__vlc_playlist.lock()
            self.__vlc_playlist.remove_index(index)
            self.__vlc_playlist.unlock()
            del self.__audio_files[index]

    def set_playback_device(self, device):
        print("switch to ", device, " for player ", self.__current_index, " current device ", self.current_device)
        self.__current_device = device or self.__default_device
        try:
            self.set_output(self.__current_device)
        except Exception as e:
            print(e)
        return self.__current_device

    def play(self, index):
        if index >= len(self.__audio_files):
            return
        audio_file = self.__audio_files[index]
        self.__vlc_player.set_playback_mode(vlc.PlaybackMode.repeat)
        index = self.__vlc_playlist.index_of_item(audio_file.vlc_file)
        if index == -1:
            return

        if self.player_status != vlc.State.Playing:
            if audio_file.volume:
                self.__volume = audio_file.volume
            self.__vlc_player.play_item_at_index(index)
            sleep(0.01)
            self.set_playback_device(audio_file.device)
            self.__current_index = index
        elif self.__current_index == index:
            self.stop()
            self.__vlc_player.play_item_at_index(index)
            sleep(0.01)
            self.set_playback_device(audio_file.device)
        else:
            self.__current_index = index
            tmp = self.__vlc_player
            self.__vlc_player = self.__vlc_player_crossfade
            self.__vlc_player_crossfade = tmp
            volume_crossfade = self.__volume
            if audio_file.volume:
                self.__volume = audio_file.volume
            elif self.__default_volume:
                self.__volume = self.__default_volume
            else:
                self.__volume = DEFAULT_VOLUME
            volume_main = 0
            self.__vlc_player.play_item_at_index(index)
            sleep(0.01)
            self.set_playback_device(audio_file.device)
            t_end = time() + self.__crossfade_time
            while time() < t_end:
                volume_main += 5
                if volume_main > self.__volume:
                    volume_main = self.__volume
                volume_crossfade -= 5
                if volume_crossfade < 0:
                    volume_crossfade = 0
                self.__vlc_player.get_media_player().audio_set_volume(volume_main)
                self.__vlc_player_crossfade.get_media_player().audio_set_volume(
                    volume_crossfade
                )
                sleep(0.2)
            self.__vlc_player_crossfade.stop()

    def incr_volume(self, file_id):
        self.__audio_files[file_id].volume += 5
        if self.__current_index == file_id and self.player_status == vlc.State.Playing:
            self.__volume = self.__audio_files[file_id].volume
            self.vlc_current_player.get_media_player().audio_set_volume(self.__volume)
        return self.__audio_files[file_id].volume

    def decr_volume(self, file_id):
        self.__audio_files[file_id].volume -= 5
        if self.__current_index == file_id and self.player_status == vlc.State.Playing:
            self.__volume = self.__audio_files[file_id].volume
            self.__vlc_player.get_media_player().audio_set_volume(self.__volume)
        return self.__audio_files[file_id].volume

    def file_volume(self, file_id):
        if self.__audio_files:
            return self.__audio_files[file_id].volume

    @property
    def volume(self):
        return self.__volume

    @volume.setter
    def volume(self, volume):
        self.__volume = volume

    def test_call(self):
        self.play(0)
        sleep(0.1)
        self.stop()
        tmp = self.__vlc_player
        self.__vlc_player = self.__vlc_player_crossfade
        self.__vlc_player_crossfade = tmp
        self.play(0)
        sleep(0.1)
        self.stop()


class LinePlayer(AudioPlayer):
    def __init__(
        self,
        device,
        default_volume=None,
        mode=None,
        crossfade=None,
        crossfade_time=None,
    ):
        self.__vlc_players = list()
        self.__audio_files = list()
        self.__vlc_playlists = list()
        self.__current_index = 0
        self.__current_device = device
        self.__player_events = list()
        self.__volume = 0
        self.__device = device

    @property
    def vlc_current_index(self):
        return self.__current_index

    @property
    def vlc_current_player(self):
        return self.__vlc_players[self.__current_index]

    @property
    def volume(self):
        pass

    @volume.setter
    def volume(self):
        pass

    @property
    def current_device(self):
        return self.__current_device

    @property
    def audio_files(self):
        return self.__audio_files

    @property
    def looping(self):
        return False

    def __set_volume_callback(self, event):
        self.vlc_current_player.get_media_player().audio_set_volume(self.__volume)

    def add_audio_file(self, audio_file, volume=None):
        if audio_file in self.__audio_files:
            print("File %s not added: already in playlist" % audio_file.file_name)
            return  # TODO Handle existing file error

        playlist = VLC_INSTANCE.media_list_new([audio_file.vlc_file])
        player = VLC_INSTANCE.media_list_player_new()
        player.set_media_list(playlist)
        self.__vlc_players.append(player)
        self.__audio_files.append(audio_file)
        self.__vlc_playlists.append(playlist)
        player.get_media_player().event_manager().event_attach(
            vlc.EventType.MediaPlayerPlaying, self.__set_volume_callback
        )

    def stop(self):
        self.vlc_current_player.pause()
        sleep(0.1)
        self.vlc_current_player.stop()

    def play(self, index):
        if index > len(self.__vlc_players):
            print(
                "Index out of range in line player: ",
                index,
                ", players: ",
                len(self.__vlc_players),
            )
            return
        if self.player_status == vlc.State.Playing:
            self.stop()
            sleep(0.9)
        self.__volume = self.__audio_files[index].volume
        self.__current_index = index
        self.__current_device = self.__audio_files[index].device
        self.vlc_current_player.play()
        sleep(0.1)
        try:
            self.set_output(self.current_device)
        except Exception as e:
            print(e)

    def incr_volume(self, file_id):
        self.__audio_files[file_id].volume += 5
        if self.__current_index == file_id and self.player_status == vlc.State.Playing:
            self.__volume = self.__audio_files[file_id].volume
            self.vlc_current_player.get_media_player().audio_set_volume(self.__volume)
        return self.__audio_files[file_id].volume

    def decr_volume(self, file_id):
        self.__audio_files[file_id].volume -= 5
        if self.__current_index == file_id and self.player_status == vlc.State.Playing:
            self.__volume = self.__audio_files[file_id].volume
            self.vlc_current_player.get_media_player().audio_set_volume(self.__volume)
        return self.__audio_files[file_id].volume

    def file_volume(self, file_id):
        if self.__audio_files:
            return self.__audio_files[file_id].volume

    def test_call(self):
        for index in range(len(self.__vlc_players)):
            self.play(index)
            sleep(0.1)
            self.stop()
