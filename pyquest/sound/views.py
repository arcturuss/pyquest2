from time import sleep

from flask import Blueprint, flash, render_template, request, send_file, jsonify
from werkzeug.utils import secure_filename

from config import AUDIO_ROOT
from pyquest.sound.manager import (
    init_audio_manager,
    SimplePlayer,
    BackgroundPlayer,
    LinePlayer,
)
import vlc


sound_manager_blp = Blueprint("sound_manager", __name__, url_prefix="/sound")
audio_manager = init_audio_manager()


def get_table():
    table = dict()

    for device in audio_manager.devices:
        table[device] = []

    for device in audio_manager.special_files:
        table[device] = []
    for index, afile in enumerate(audio_manager.files):
        table[afile.location].append(
            dict(index=index, name=afile.filename, volume=afile.volume)
        )
    return table


@sound_manager_blp.route("/table")
def list_audio_files():
    simple_players = dict()
    background_players = dict()
    line_players = dict()
    for key, val in audio_manager.players.items():
        if isinstance(val, SimplePlayer):
            device = val.device
            if device not in simple_players:
                simple_players[device] = []
            if val.audio_file:
                file_name = val.audio_file.file_name
            else:
                file_name = None
            simple_players[device].append(
                {"name": key, "file_name": file_name, "volume": val.volume}
            )
        elif isinstance(val, BackgroundPlayer):
            background_players[key] = {"audio_files": val.audio_files}
        elif isinstance(val, LinePlayer):
            line_players[key] = {"audio_files": val.audio_files}
    return render_template(
        "sound/table.html",
        simple_players=simple_players,
        background_players=background_players,
        line_players=line_players,
    )


@sound_manager_blp.route("/form")
def get_submit_form():
    files = [afile.file_name for afile in audio_manager.files.values()]
    return render_template(
        "sound/form.html", files=files, devices=sorted(audio_manager.devices)
    )


@sound_manager_blp.route("/files")
def file_list():
    file_list = [x.file_name for x in audio_manager.files.values()]
    return file_list


@sound_manager_blp.route("/file", methods=["POST"], defaults={"name": None})
@sound_manager_blp.route("/file/<string:name>", methods=["GET", "DELETE"])
def audio_file(name):
    if request.method == "DELETE":
        audio_manager.remove_sound_file(name)
        return "", 204

    if request.method == "GET":
        return (
            send_file(str(audio_manager.files[name].absolute_path), as_attachment=True),
            200,
        )
    if request.method == "POST":
        if "file" not in request.files:
            flash("No file part")
            return
        file_h = request.files["file"]
        file_name = secure_filename(file_h.filename)
        file_path = AUDIO_ROOT.joinpath(file_name)
        file_h.save(str(file_path))
        audio_manager.add_audio_file(file_path)
        return "", 201


@sound_manager_blp.route("/player", methods=["POST"])
def add_player():
    # print(request.form)
    audio_manager.add_player(
        request.form["device"], request.form["playerName"], request.form["type"]
    )
    return "", 201


@sound_manager_blp.route("/player/add/file", methods=["POST"])
def add_file_to_player():
    player = audio_manager.players[request.form["player"]]
    audio_file = audio_manager.files[request.form["audio_file"]]
    player.add_audio_file(audio_file)
    return "", 201


@sound_manager_blp.route("/player/<string:name>/play")
def play_audio_file(name):
    audio_manager.players[name].play()
    return "", 204


@sound_manager_blp.route("/player/<string:name>/stop")
def stop_audio_file(name):
    audio_manager.players[name].stop()
    return "", 204


@sound_manager_blp.route("/player/<string:name>/volume_up")
def volume_up_audio_player(name):
    audio_manager.players[name].incr_volume()
    audio_manager.dump_config()
    return "", 204


@sound_manager_blp.route("/player/<string:name>/volume_down")
def volume_down_audio_player(name):
    audio_manager.players[name].decr_volume()
    audio_manager.dump_config()
    return "", 204


@sound_manager_blp.route("/player/<string:player>/<int:file_id>/volume_up")
def volume_up_audio_file(player, file_id):
    audio_manager.players[player].incr_volume(file_id)
    audio_manager.dump_config()
    return "", 204


@sound_manager_blp.route("/player/<string:player>/<int:file_id>/volume_down")
def volume_down_audio_file(player, file_id):
    audio_manager.players[player].decr_volume(file_id)
    audio_manager.dump_config()
    return "", 204


@sound_manager_blp.route("/player/<string:player>/<int:file_id>/volume")
def get_audio_file_volume(player, file_id):
    return str(audio_manager.players[player].file_volume(file_id)), 200


@sound_manager_blp.route("/player/<string:name>/mute")
def mute_audio_file(name):
    audio_manager.players[name].toggle_mute()
    return "", 204


@sound_manager_blp.route("/player/<string:name>/volume")
def get_audio_player_volume(name):
    return str(audio_manager.players[name].volume), 200


# @sound_manager_blp.route("/player/notPlaying")
# def audio_files_not_playting():
#     not_playing_file_ixs = list()
#     for index, afile in enumerate(audio_manager.players):
#         if afile.not_playing:
#             not_playing_file_ixs.append(index)
# return jsonify(not_playing_file_ixs)


@sound_manager_blp.route("/player/notPlaying/simple")
def simple_not_playing():
    simple_players = list()
    for key, val in audio_manager.players.items():
        if isinstance(val, SimplePlayer):
            if val.player_status != vlc.State.Playing:
                simple_players.append(key)
    return jsonify(simple_players)


@sound_manager_blp.route("/player/notPlaying/list")
def list_not_playing():
    list_players = dict()
    for key, val in audio_manager.players.items():
        if isinstance(val, LinePlayer) or isinstance(val, BackgroundPlayer):
            if val.player_status == vlc.State.Playing:
                list_players[key] = val.vlc_current_index
            else:
                list_players[key] = -1
    return jsonify(list_players)


@sound_manager_blp.route("/player/<string:name>/<int:index>/play")
def bg_play_audio_file(name, index):
    audio_manager.players[name].play(index)
    return "", 204


@sound_manager_blp.route("/player/testcall")
def player_test_call():
    audio_manager.test_call()
    return "", 204
