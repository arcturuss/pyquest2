#!/usr/bin/env python
from flipflop import WSGIServer
from pyquest import app

WSGIServer(app).run()
