import threading
import atexit

from flask import Flask

from pyquest.serial import interrupt, start_serial, serial_manager_blp
from pyquest.sound import sound_manager_blp


def create_app():
    app = Flask(__name__)
    app.register_blueprint(serial_manager_blp)
    app.register_blueprint(sound_manager_blp)

    start_serial()
    atexit.register(interrupt)
    return app


app = create_app()

# app = Flask(__name__)

from pyquest import views
