from struct import pack

from crcmod.predefined import mkCrcFun


def calc_crc(input_bytearray):
    return mkCrcFun("crc-8-maxim")(input_bytearray)


class ToDevice:

    def __init__(self, address, command):
        self.head = b"\x02\x02"
        self.address = address.encode()
        self.command = command.encode()

    def pack(self):
        header = bytearray(self.address + self.command)
        header.append(calc_crc(header))
        return self.head + header


class FromDevice:

    def __init__(self, packet):
        print("packet " + str(packet))
        if calc_crc(packet) != 0:
            raise ValueError
        # криво, но пока хз как это сделать иначе
        try:
            self.address = pack(
                'BBBBBB', packet[0], packet[1], packet[2], packet[3], packet[4], packet[5])
            self.reply = pack(
                'BBBBBB', packet[6], packet[7], packet[8], packet[9], packet[10], packet[11])
        except IndexError:
            raise ValueError

class ToDeviceSimple():
    def __init__(self, command):
        self.command = command

    def pack(self):
        return self.command
