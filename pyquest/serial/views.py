import json
from queue import Queue
import queue
from time import sleep

from flask import Blueprint, request

from pyquest.serial.device import QuestSerial
from pyquest.serial.message import ToDevice, ToDeviceSimple
from pyquest.sound import audio_manager

import subprocess


serial_manager_blp = Blueprint("serial_manager", __name__, url_prefix="/serial")


def interrupt():
    qs.join(0)


def start_serial():
    qs.start()


devices = {
    'shoot.': {
        'status': 'none', # last known device status
        'active': False, # False if device is non-responsive
        'lastresponse': '', # from device
        'htmlid': 'shoot', # GUI info
        'htmlclass': 'btn-none', # GUI info
        'htmldata': { # id : class
            'shooter-lock': 'status_off',
        },               
    },
    'train.': {
        'status': 'none', # last known device status
        'active': False, # False if device is non-responsive
        'lastresponse': '', # from device
        'htmlid': 'train', # GUI info
        'htmlclass': 'btn-none', # GUI info
        'htmldata': { # id : class
            'train-smoke': 'status_off',
            'train-smoke-on': 'status_off',
            'train-vent': 'status_off',
            'train-lamp': 'status_off',
            'train-uv': 'status_off',
            'train-light': 'status_on',
        },           
    },
    'light.': {
        'status': 'none', # last known device status
        'active': False, # False if device is non-responsive
        'lastresponse': '', # from device
        'htmlid': 'light', # GUI info
        'htmlclass': 'btn-none', # GUI info     
        'htmldata': { # id : class
            'light-cards': 'status_off',
        },   
    },
    'jail..': {
        'status': 'none', # last known device status
        'active': False, # False if device is non-responsive
        'lastresponse': '', # from device
        'htmlid': 'jail', # GUI info
        'htmlclass': 'btn-none', # GUI info
        'htmldata': { # id : class
            'jail-vent': 'status_off',
            'jail-lock': 'status_off',
            'jail-card': 'status_off',

        },
    },   
    'horn..': {
        'status': 'none', # last known device status
        'active': False, # False if device is non-responsive
        'lastresponse': '', # from device
        'htmlid': 'horn', # GUI info
        'htmlclass': 'btn-none', # GUI info
        'htmldata': { # id : class
            'horn-lock': 'status_off',
            'horn-lamp1': 'status_off',
            'horn-lamp2': 'status_off',
            'horn-lamp3': 'status_off',
        },
    },  
    'tools.': {
        'status': 'none', # last known device status
        'active': False, # False if device is non-responsive
        'lastresponse': '', # from device
        'htmlid': 'tools', # GUI info
        'htmlclass': 'btn-none', # GUI info
        'htmldata': { # id : class
            'tools-lock': 'status_off',
            'tools-p0': 0,
            'tools-p1': 0,
            'tools-p2': 0,
            'tools-p3': 0,
        },        
    },
    'dance.': {
        'status': 'none', # last known device status
        'active': False, # False if device is non-responsive
        'lastresponse': '', # from device
        'htmlid': 'dance', # GUI info
        'htmlclass': 'btn-none', # GUI info
        'htmldata': { # id : class
            'dance-on': 'status_off',
            'dance-lock': 'status_off',
            'dance-start': 'status_off',

        },
    },   
    'horse.': {
        'status': 'none', # last known device status
        'active': False, # False if device is non-responsive
        'lastresponse': '', # from device
        'htmlid': 'horseshoes', # GUI info
        'htmlclass': 'btn-none', # GUI info
        'htmldata': { # id : class
            'horse-lock': 'status_off',
            'horse-light': 'status_off',
        },
    },   
}

task_queue = Queue()
data_queue = Queue()

qs = QuestSerial(task_queue, data_queue, devices)

# ------------------------------------

@serial_manager_blp.route('/micSetup')
def sysMicSetup():
#    logger.debug("pulseaudio: microphone config")
    subprocess.run(["/usr/bin/pactl", "unload-module", "module-loopback"])
    subprocess.run(["/usr/bin/pactl", "load-module", "module-loopback", "source=alsa_input.pci-0000_00_1f.5.analog-stereo", "sink=ambient-room-shed-train", "latency_msec=5", "channels=1"])
    subprocess.run(["/usr/bin/pactl", "set-source-volume", "alsa_input.pci-0000_00_1f.5.analog-stereo", "20000"])
    return ('', 200)

@serial_manager_blp.route('/reboot')
def sysReboot():
#    logger.debug("reboot requested")
    subprocess.run(["sudo", "/sbin/reboot"])
    return ('', 200)

@serial_manager_blp.route('/poweroff')
def sysPoweroff():
#    logger.debug("poweroff requested")
    subprocess.run(["sudo", "/sbin/shutdown", "-h", "now"])
    return ('', 200)

# ------------------------------------

@serial_manager_blp.route('/allLightOff')
def allLightOff():
    task_queue.put(ToDevice('light.', 'lgtoff'))
    task_queue.put(ToDevice('train.', 'lgtoff'))
    return ('', 204)

@serial_manager_blp.route('/allLightOn')
def allLightOn():
    task_queue.put(ToDevice('light.', 'lghton'))
    task_queue.put(ToDevice('train.', 'lghton'))
    return ('', 204)

# ------------------------------------

@serial_manager_blp.route('/response')
def urlUpdate():
    global devices
    device = request.args.get("device", "")
    print("d: " + device)
    reply = request.args.get("reply", "")
    print("s: " + reply)
    command = request.args.get("command", "")
    parseUpdate(device, command, reply)
    return ("", 200)


@serial_manager_blp.route("/getUpdate")
def returnUpdate():
    global devices
    for devname in devices:  # эти три строчки ломают отправку в serial полностью
        task_queue.put(
            ToDevice(devname, "status")
        )  # наверное из-за какой-то тупой ошибки
        sleep(0.05)  # wait for answer                    # я не знаю
    while not data_queue.empty():
        try:
            item = data_queue.get_nowait()
        except queue.Empty:
            break
        print("Got update")
        parseUpdate(item['device'], item['command'], item['reply'])

    return (json.dumps(devices), 200)

@serial_manager_blp.route('/testSoundTrain')
def testSoundTrain():
    audio_manager.players["train_sound"].play()
    return ('', 204)

@serial_manager_blp.route('/testSoundArrow')
def testSoundArrow():
    audio_manager.players["arrow_sound"].play()
    return ('', 204)    

@serial_manager_blp.route('/testOn')
def test2():
    task_queue.put(ToDeviceSimple(b"deviceon\n"))
    return ("", 204)


@serial_manager_blp.route("/testOff")
def test3():
    task_queue.put(ToDeviceSimple(b"deviceoff\n"))
    return ("", 204)


@serial_manager_blp.route("/testStatus")
def test4():
    # send 'status' to all devices
    # task_queue.put(ToDeviceSimple(b'test..status'))
    task_queue.put(ToDevice("test..", "status"))
    return ("", 204)


@serial_manager_blp.route("/resetAll")
def resetAll():
    # send 'status' to all devices
    # task_queue.put(ToDeviceSimple(b'test..status'))
    # task_queue.put(ToDevice('test..', 'reset.'))
    task_queue.put(ToDevice('shoot.', 'reset.'))
    task_queue.put(ToDevice('train.', 'reset.'))
    task_queue.put(ToDevice('light.', 'reset.'))
    task_queue.put(ToDevice('jail..', 'reset.'))
    task_queue.put(ToDevice('horn..', 'reset.'))
    task_queue.put(ToDevice('tools.', 'reset.'))
    task_queue.put(ToDevice('dance.', 'reset.'))
    return ('', 204)    

def parseUpdate(device, command, reply):
    # device = str(device)
    # command = str(command)
    # reply = str(reply)
    print("dev: " + str(device))
    print("cmd: " + str(command))
    print("rsp: " + str(reply))
    if device == b"test..":
        print("got dev")
        if reply is not "":
            devices["test.."]["lastresponse"] = str(reply)
            devices["test.."]["active"] = True
        if reply == "error.":
            devices["test.."]["status"] = "error"
            devices["test.."]["htmlclass"] = "btn-danger"
        elif reply == "isoff.":
            devices["test.."]["status"] = "off"
            devices["test.."]["htmlclass"] = "btn-warning"
        elif reply == "ison..":
            devices["test.."]["status"] = "on"
            devices["test.."]["htmlclass"] = "btn-success"
        elif reply == b"tstrsp":
            devices["test.."]["htmlclass"] = "btn-info active"
            print("test respnse received")
        else:
            devices['test..']['status'] = 'unrecognized error'
            devices['test..']['htmlclass'] = 'btn-danger'
    elif device == b'shoot.':
        if reply is not '':
            devices['shoot.']['lastresponse'] = str(reply)
            devices['shoot.']['active'] = True
        else :
            devices['shoot.']['status'] = 'link error'
            devices['shoot.']['htmlclass'] = 'btn-warning'
        if reply == b'error.':
            devices['shoot.']['status'] = 'error'
            devices['shoot.']['htmlclass'] = 'btn-danger'
        elif reply == b'locked':
            devices['shoot.']['status'] = 'locked'
            devices['shoot.']['htmlclass'] = 'btn-info active'
            devices['shoot.']['htmldata']['shooter-lock'] = 'status_on';
        elif reply == b'unlckd':
            devices['shoot.']['status'] = 'unlckd'
            devices['shoot.']['htmlclass'] = 'btn-success'
            devices['shoot.']['htmldata']['shooter-lock'] = 'status_off';
        elif reply == b'gotsht':
            devices['shoot.']['status'] = 'Shoot detected!'
            devices['shoot.']['htmlclass'] = 'btn-warning'
        elif reply == b'ison..':
            devices['shoot.']['status'] = 'on'
            devices['shoot.']['htmlclass'] = 'btn-info active'
        else:
            devices['shoot.']['status'] = 'unrecognized error'
            devices['shoot.']['htmlclass'] = 'btn-danger'
    elif device == b'train.':
        if reply is not '':
            devices['train.']['lastresponse'] = str(reply)
            devices['train.']['active'] = True
        else :
            devices['train.']['status'] = 'link error'
            devices['train.']['htmlclass'] = 'btn-warning'

        # переписать на правильную проверку
        if reply[0] != ord('0') and reply[0] != ord('l') and reply[0] != ord('2'): 
            devices['train.']['status'] = 'comm error'
            devices['train.']['htmlclass'] = 'btn-warning'
        else :
            devices['train.']['status'] = 'comm ok'
            devices['train.']['htmlclass'] = 'btn-none'
            if reply[0] == ord('0'):
                devices['train.']['htmldata']['train-smoke'] = 'status_off';
            if reply[0] == ord('1'):
                devices['train.']['htmldata']['train-smoke'] = 'status_mid';
            if reply[0] == ord('2'):
                devices['train.']['htmldata']['train-smoke'] = 'status_on'; 

            if reply[1] == ord('0'):
                devices['train.']['htmldata']['train-smoke-on'] = 'status_off';
            if reply[1] == ord('1'):
                devices['train.']['htmldata']['train-smoke-onn'] = 'status_on';

            if reply[3] == ord('0'):
                devices['train.']['htmldata']['train-vent'] = 'status_off';
            if reply[3] == ord('1'):
                devices['train.']['htmldata']['train-vent'] = 'status_on';

            if reply[2] == ord('0'):
                devices['train.']['htmldata']['train-lamp'] = 'status_off';
            if reply[2] == ord('1'):
                devices['train.']['htmldata']['train-lamp'] = 'status_on';

            if reply[4] == ord('0'):
                devices['train.']['htmldata']['train-uv'] = 'status_off';
            if reply[4] == ord('1'):
                devices['train.']['htmldata']['train-uv'] = 'status_on';

            if reply[5] == ord('0'):
                devices['train.']['htmldata']['train-light'] = 'status_off';
            if reply[5] == ord('1'):
                devices['train.']['htmldata']['train-light'] = 'status_on';


    elif device == b'light.':
        if reply is not '':
            devices['light.']['lastresponse'] = str(reply)
            devices['light.']['active'] = True
        else :
            devices['light.']['status'] = 'link error'
            devices['light.']['htmlclass'] = 'btn-warning'
        if reply == b'cards.':
            devices['light.']['status'] = 'Cards set!'
            devices['light.']['htmlclass'] = 'btn-info'
            devices['light.']['htmldata']['light-cards'] = 'status_on'
        else:
            devices['light.']['htmldata']['light-cards'] = 'status_off'
        if reply == b'ison..':
            devices['light.']['status'] = 'on'
            devices['light.']['htmlclass'] = 'btn-info'
        elif reply == b'isoff.':
            devices['light.']['status'] = 'off'
            devices['light.']['htmlclass'] = 'btn-danger'
        elif reply == b'vhigh.':
            devices['light.']['status'] = 'voltage: high. Недавно заряжен'
            devices['light.']['htmlclass'] = 'btn-success active'
        elif reply == b'vmid..':
            devices['light.']['status'] = 'voltage: mid. Батарея в норме'
            devices['light.']['htmlclass'] = 'btn-success active'
        elif reply == b'vlow..':
            devices['light.']['status'] = 'voltage: low. Скоро на зарядку'
            devices['light.']['htmlclass'] = 'btn-warning'
        elif reply == b'voff..':
            devices['light.']['status'] = 'voltage: critical. Пора зарядить!'
            devices['light.']['htmlclass'] = 'btn-danger active'            
        elif reply == b're-set':
            devices['light.']['status'] = 'reset'
            devices['light.']['htmlclass'] = 'btn-warning active'
        elif reply == b'rferr.':
            devices['light.']['status'] = 'Radio error'
            devices['light.']['htmlclass'] = 'btn-warning active'
        elif reply == b'error.':
            devices['light.']['status'] = 'Error!'
            devices['light.']['htmlclass'] = 'btn-danger'

        else:
            devices['light.']['status'] = 'unrecognized error'
            devices['light.']['htmlclass'] = 'btn-danger' 
    elif device == b'jail..':
        if reply is not '':
            devices['jail..']['lastresponse'] = str(reply)
            devices['jail..']['active'] = True
        else :
            devices['jail..']['status'] = 'link error'
            devices['jail..']['htmlclass'] = 'btn-warning'
        # if reply[0] != 118 or reply[2] != 108 or reply[4] != 99: # 'v', 'l', 'c'
        if reply[0] != ord('v') or reply[2] != ord('l') or reply[4] != ord('c'): 
            devices['jail..']['status'] = 'comm error'
            devices['jail..']['htmlclass'] = 'btn-warning'
        else :
            devices['jail..']['status'] = 'comm ok'
            devices['jail..']['htmlclass'] = 'btn-none'
            if reply[1] == ord('1'):
                devices['jail..']['htmldata']['jail-vent'] = 'status_on';
            if reply[1] == ord('0'):
                devices['jail..']['htmldata']['jail-vent'] = 'status_off';
            if reply[3] == ord('1'):
                devices['jail..']['htmldata']['jail-lock'] = 'status_on';
            if reply[3] == ord('0'):
                devices['jail..']['htmldata']['jail-lock'] = 'status_off';
            if reply[5] == ord('1'):
                devices['jail..']['htmldata']['jail-card'] = 'status_on';
            if reply[5] == ord('0'):
                devices['jail..']['htmldata']['jail-card'] = 'status_off';
    elif device == b'horn..':
        if reply is not '':
            devices['horn..']['lastresponse'] = str(reply)
            devices['horn..']['active'] = True
        else :
            devices['horn..']['status'] = 'link error'
            devices['horn..']['htmlclass'] = 'btn-warning'
        # if reply[0] != 118 or reply[2] != 108 or reply[4] != 99: # 'v', 'l', 'c'
        if reply == b're-set':
            devices['horn..']['status'] = 'reset'
            devices['horn..']['htmlclass'] = 'btn-success active'        
        elif reply[0] != ord('l') or reply[2] != ord('L'): 
            devices['horn..']['status'] = 'comm error'
            devices['horn..']['htmlclass'] = 'btn-warning'
        else :
            devices['horn..']['status'] = 'comm ok'
            devices['horn..']['htmlclass'] = 'btn-none'
            if reply[1] == ord('1'):
                devices['horn..']['htmldata']['horn-lock'] = 'status_on';
            if reply[1] == ord('0'):
                devices['horn..']['htmldata']['horn-lock'] = 'status_off';
            if reply[3] == ord('1'):
                devices['horn..']['htmldata']['horn-lamp1'] = 'status_on';
            if reply[3] == ord('0'):
                devices['horn..']['htmldata']['horn-lamp1'] = 'status_off';                
            if reply[4] == ord('1'):
                devices['horn..']['htmldata']['horn-lamp2'] = 'status_on';
            if reply[4] == ord('0'):
                devices['horn..']['htmldata']['horn-lamp2'] = 'status_off';                                
            if reply[5] == ord('1'):
                devices['horn..']['htmldata']['horn-lamp3'] = 'status_on';
            if reply[5] == ord('0'):
                devices['horn..']['htmldata']['horn-lamp3'] = 'status_off';                
    elif device == b'tools.':
        if reply is not '':
            devices['tools.']['lastresponse'] = str(reply)
            devices['tools.']['active'] = True
        else :
            devices['tools.']['status'] = 'link error'
            devices['tools.']['htmlclass'] = 'btn-warning'
        # if reply[0] != 118 or reply[2] != 108 or reply[4] != 99: # 'v', 'l', 'c'
        if reply == b're-set':
            devices['tools.']['status'] = 'reset'
            devices['tools.']['htmlclass'] = 'btn-success active'        
        elif reply[0] != ord('U') and reply[0] != ord('L'): 
            devices['tools.']['status'] = 'comm error'
            devices['tools.']['htmlclass'] = 'btn-warning'
        else :
            devices['tools.']['status'] = 'comm ok'
            devices['tools.']['htmlclass'] = 'btn-none'
            if reply[0] == ord('U'):
                devices['tools.']['htmldata']['tools-lock'] = 'status_off'
            if reply[0] == ord('L'):
                devices['tools.']['htmldata']['tools-lock'] = 'status_on'
            devices['tools.']['htmldata']['tools-p0'] = reply[2]
            devices['tools.']['htmldata']['tools-p1'] = reply[3]
            devices['tools.']['htmldata']['tools-p2'] = reply[4]
            devices['tools.']['htmldata']['tools-p3'] = reply[5]

    elif device == b'dance.':
        if reply is not '':
            devices['dance.']['lastresponse'] = str(reply)
            devices['dance.']['active'] = True
        else :
            devices['dance.']['status'] = 'link error'
            devices['dance.']['htmlclass'] = 'btn-warning'
        # if reply[0] != 118 or reply[2] != 108 or reply[4] != 99: # 'v', 'l', 'c'
        if reply[0] != ord('s') or reply[2] != ord('l') or reply[4] != ord('t'): 
            devices['dance.']['status'] = 'comm error'
            devices['dance.']['htmlclass'] = 'btn-warning'
        else :
            devices['dance.']['status'] = 'comm ok'
            devices['dance.']['htmlclass'] = 'btn-none'
            if reply[1] == ord('0'):
                devices['dance.']['htmldata']['dance-on'] = 'status_off';
                devices['dance.']['htmldata']['dance-start'] = 'status_off';
            if reply[1] == ord('1'):
                devices['dance.']['htmldata']['dance-on'] = 'status_on';
                devices['dance.']['htmldata']['dance-start'] = 'status_off';
            if reply[1] == ord('2'):
                devices['dance.']['htmldata']['dance-start'] = 'status_on';
            if reply[3] == ord('0'):
                devices['dance.']['htmldata']['dance-lock'] = 'status_off';
            if reply[3] == ord('1'):
                devices['dance.']['htmldata']['dance-lock'] = 'status_on';
#            if reply[5] == ord('1'):
#                devices['dance.']['htmldata']['jail-card'] = 'status_on';
#            if reply[5] == ord('0'):
#                devices['dance.']['htmldata']['jail-card'] = 'status_off';

    elif device == b'horse.':
        if reply is not '':
            devices['horse.']['lastresponse'] = str(reply)
            devices['horse.']['active'] = True
        else :
            devices['horse.']['status'] = 'link error'
            devices['horse.']['htmlclass'] = 'btn-warning'
        if reply == b're-set':
            devices['horse.']['status'] = 'reset'
            devices['horse.']['htmlclass'] = 'btn-success active'   
        elif reply == b'opened':
            devices['horse.']['status'] = 'opened'
            devices['horse.']['htmlclass'] = 'btn-success active'    
            devices['horse.']['htmldata']['horse-lock']  = 'status_off'
            devices['horse.']['htmldata']['horse-light'] = 'status_on'
        elif reply == b'closed':
            devices['horse.']['status'] = 'closed'
            devices['horse.']['htmlclass'] = 'btn-warning active'    
            devices['horse.']['htmldata']['horse-lock']  = 'status_on'
            devices['horse.']['htmldata']['horse-light'] = 'status_off'
        else: 
            devices['horse.']['status'] = 'comm error'
            devices['horse.']['htmlclass'] = 'btn-warning'


    elif device == 'none':
        pass


@serial_manager_blp.route("/shooterLock")
def shooterLock():
    task_queue.put(ToDevice("shoot.", "lock.."))
    print("lock..")
    return ("", 204)


@serial_manager_blp.route("/shooterUnlock")
def shooterUnlock():
    task_queue.put(ToDevice("shoot.", "unlock"))
    print("unlock")
    return ("", 204)


@serial_manager_blp.route("/shooterShot")
def shooterShot():
    task_queue.put(ToDevice('shoot.', 'doshot'))
    return('', 204)

@serial_manager_blp.route('/shooterShot2')
def shooterShot2():
    task_queue.put(ToDevice('shoot.', 'shot2.'))
    return('', 204)    

@serial_manager_blp.route('/shooterSoundRicochet')
def shooterSoundRicochet():
    #task_queue.put(ToDevice('shoot.', 'shot2.'))
    audio_manager.players["shooter_sound"].play()
    return('', 204)    

@serial_manager_blp.route('/shooterSound1')
def shooterSound1():
    task_queue.put(ToDevice("shoot.", "voice1"))
    return ("", 204)


@serial_manager_blp.route("/shooterSound2")
def shooterSound2():
    task_queue.put(ToDevice("shoot.", "voice2"))
    return ("", 204)


@serial_manager_blp.route("/shooterSound3")
def shooterSound3():
    task_queue.put(ToDevice("shoot.", "voice3"))
    return ("", 204)


# @serial_manager_blp.route('/shooterShotSound')
# def shooterShotSound():
#     # task_queue.put(ToDevice(b'shoot.', b'doshot'))
#     # global shot_player
#     # shot_player.stop()
#     # shot_player.set_media(shot_sound)
#     # shot_player.play()
#     # time.sleep(0.01)
#     # shot_player.audio_output_device_set(None, b'one-left')
#     return('', 204)    

@serial_manager_blp.route('/shooterStart')
def shooterStart():
    task_queue.put(ToDevice('shoot.', 'start.'))
    task_queue.put(ToDevice('horse.', 'lock..'))
    print('shooter - start');
    print('horse lock');
    return('', 204)   

@serial_manager_blp.route('/shooterEnd')
def shooterEnd():
    task_queue.put(ToDevice('shoot.', 'end...'))
    # task_queue.put(ToDevice('horse.', 'unlock'))
    print('shooter end');
    print('horse unlock');
    return('', 204)   

@serial_manager_blp.route('/shooterReset')
def shooterReset():
    task_queue.put(ToDevice("shoot.", "reset."))
    print("reset.")
    return ("", 204)


# -------------------------------------------------------------------


@serial_manager_blp.route("/trainReset")
def trainReset():
    task_queue.put(ToDevice("train.", "reset."))
    print("train reset.")
    return ("", 204)


@serial_manager_blp.route("/trainOn")
def trainOn():
    task_queue.put(ToDevice("train.", "turnon"))
    print("train on")
    return ("", 204)


@serial_manager_blp.route("/trainOff")
def trainOff():
    task_queue.put(ToDevice("train.", "trnoff"))
    print("train off")
    return ("", 204)


@serial_manager_blp.route("/trainSmoke")
def trainSmoke():
    task_queue.put(ToDevice('train.', 'smoke.'))
    print('train smoke');
    return('', 204)       

@serial_manager_blp.route('/trainSmokeOnly')
def trainSmokeOnly():
    task_queue.put(ToDevice('train.', 'smoke2'))
    print('train smoke');
    return('', 204)          

@serial_manager_blp.route('/trainSmokeWithSound')
def trainSmokeWithSound():
    task_queue.put(ToDevice('train.', 'smoke.'))
    audio_manager.players["train_sound"].play()
    print('train smoke');
    return('', 204)   

@serial_manager_blp.route('/trainUvOn')
def trainUvOn():
    task_queue.put(ToDevice("train.", "uvon.."))
    task_queue.put(ToDevice("light.", "lgtoff"))
    print("train uv on")
    return ("", 204)


@serial_manager_blp.route("/trainUvOff")
def trainUvOff():
    task_queue.put(ToDevice('train.', 'uvoff.'))
    task_queue.put(ToDevice('light.', 'lgtcnc'))
    print('train uv off');
    return('', 204)       

@serial_manager_blp.route('/trainLampOn')
def trainLampOn():
    task_queue.put(ToDevice('train.', 'lampon'))
    print('train Lamp on');
    return('', 204)       

@serial_manager_blp.route('/trainLampOff')
def trainLampOff():
    task_queue.put(ToDevice('train.', 'lmpoff'))
    print('train Lamp off');
    return('', 204)      

@serial_manager_blp.route('/trainVentOn')
def trainVentOn():
    task_queue.put(ToDevice('train.', 'venton'))
    print('train Vent on');
    return('', 204)       

@serial_manager_blp.route('/trainVentOff')
def trainVentOff():
    task_queue.put(ToDevice('train.', 'vntoff'))
    print('train Vent off');
    return('', 204)      

@serial_manager_blp.route('/trainLightOn')
def trainLightOn():
    task_queue.put(ToDevice('train.', 'lghton'))
    print('train Light on');
    return('', 204)       

@serial_manager_blp.route('/trainLightOff')
def trainLightOff():
    task_queue.put(ToDevice('train.', 'lgtoff'))
    print('train Light off');
    return('', 204)      

# -------------------------------------------------------------------


@serial_manager_blp.route("/lightReset")
def lightReset():
    task_queue.put(ToDevice("light.", "reset."))
    print("light reset.")
    return ("", 204)


@serial_manager_blp.route("/lightOn")
def lightOn():
    task_queue.put(ToDevice("light.", "lghton"))
    print("light on")
    return ("", 204)


@serial_manager_blp.route("/lightOff")
def lightOff():
    task_queue.put(ToDevice("light.", "lgtoff"))
    print("light off")
    return ("", 204)


@serial_manager_blp.route("/lightCancel")
def lightCancel():
    task_queue.put(ToDevice("light.", "lgtcnc"))
    print("light cancel")
    return ("", 204)


@serial_manager_blp.route("/lightArrow")
def lightArrow():
    task_queue.put(ToDevice('light.', 'arrow.'))
    audio_manager.players["arrow_sound"].play()
    print('light arrow');
    return('', 204)   

# -------------------------------------------------------------------


@serial_manager_blp.route("/jailReset")
def jailReset():
    task_queue.put(ToDevice("jail..", "reset."))
    print("jail reset.")
    return ("", 204)


@serial_manager_blp.route("/jailVentOn")
def jailVentOn():
    task_queue.put(ToDevice("jail..", "venton"))
    print("jail vent on")
    return ("", 204)


@serial_manager_blp.route("/jailVentOff")
def jailVentOff():
    task_queue.put(ToDevice("jail..", "vntoff"))
    print("jail vent off")
    return ("", 204)


@serial_manager_blp.route("/jailLockOn")
def jailLockOn():
    task_queue.put(ToDevice("jail..", "lock.."))
    print("jail lock")
    return ("", 204)


@serial_manager_blp.route("/jailLockOff")
def jailLockOff():
    task_queue.put(ToDevice('jail..', 'unlock'))
    print('jail unlock');
    return('', 204)   
# -------------------------------------------------------------------

@serial_manager_blp.route('/hornReset')
def hornReset():
    task_queue.put(ToDevice('horn..', 'reset.'))
    print('horn reset.');
    return('', 204)        

@serial_manager_blp.route('/hornLock')
def hornLock():
    task_queue.put(ToDevice('horn..', 'lock..'))
    print('horn lock');
    return('', 204)   

@serial_manager_blp.route('/hornUnlock')
def hornUnlock():
    task_queue.put(ToDevice('horn..', 'unlock'))
    print('horn unlock');
    return('', 204)     

# -------------------------------------------------------------------

@serial_manager_blp.route('/toolsReset')
def toolsReset():
    task_queue.put(ToDevice('tools.', 'reset.'))
    print('tools reset.');
    return('', 204)        

@serial_manager_blp.route('/toolsLock')
def toolsLock():
    task_queue.put(ToDevice('tools.', 'lock..'))
    print('tools lock');
    return('', 204)   

@serial_manager_blp.route('/toolsUnlock')
def toolsUnlock():
    task_queue.put(ToDevice('tools.', 'unlock'))
    print('tools unlock');
    return('', 204)   

# -------------------------------------------------------------------

@serial_manager_blp.route('/danceReset')
def danceReset():
    task_queue.put(ToDevice('dance.', 'reset.'))
    print('dance reset.');
    return('', 204)        

@serial_manager_blp.route('/danceLock')
def danceLock():
    task_queue.put(ToDevice('dance.', 'lock..'))
    print('dance lock');
    return('', 204)   

@serial_manager_blp.route('/danceUnlock')
def danceUnlock():
    task_queue.put(ToDevice('dance.', 'unlock'))
    print('dance unlock');
    return('', 204)   

@serial_manager_blp.route('/danceOn')
def danceOn():
    task_queue.put(ToDevice('dance.', 'turnon'))
    print('dance on');
    return('', 204)   

@serial_manager_blp.route('/danceOff')
def danceOff():
    task_queue.put(ToDevice('dance.', 'trnoff'))
    print('dance off');
    return('', 204)   

@serial_manager_blp.route('/danceStart')
def danceStart():
    task_queue.put(ToDevice('dance.', 'start.'))
    print('dance start');
    return('', 204)   

@serial_manager_blp.route('/danceStop')
def danceStop():
    task_queue.put(ToDevice('dance.', 'stop..'))
    print('dance stop');
    return('', 204)   

# -------------------------------------------------------------------

@serial_manager_blp.route('/horseReset')
def horseReset():
    task_queue.put(ToDevice('horse.', 'reset.'))
    print('horse reset.');
    return('', 204)        

@serial_manager_blp.route('/horseLock')
def horseLock():
    task_queue.put(ToDevice('horse.', 'lock..'))
    print('horse lock');
    return('', 204)   

@serial_manager_blp.route('/horseUnlock')
def horseUnlock():
    task_queue.put(ToDevice('horse.', 'unlock'))
    print('horse unlock');
    return('', 204)   


# def signal_handler(signal, frame):
#     ser.join()
#     sys.exit(0)


# signal.signal(signal.SIGINT, signal_handler)
# devices = {
#     'test..': {
#         'status': 'none', # last known device status
#         'active': False, # False if device is non-responsive
#         'lastresponse': '', # from device
#         'htmlid': 'test4', # GUI info
#         'htmlclass': 'btn-warning', # GUI info
#     }
# }

# logger.debug("init")
# print(codecs.encode("123ӡӢӣӤӥӦӧӨөӪӫӬӭӮӯ04F0ӰӱӲӳӴӵӶӷӸӹӺӻӼӽӾӿ0500ԀԁԂԃԄԅԆԇ321", "utf-8"))
# logger.debug("123ӡӢӣӤӥӦӧӨөӪӫӬӭӮӯ04F0ӰӱӲӳӴӵӶӷӸӹӺӻӼӽӾӿ0500ԀԁԂԃԄԅԆԇ321")

# task_queue = Queue()
# data_queue = Queue()
# qs = QuestSerial(task_queue, data_queue, devices)
# qs.start()
