import logging
import time
from glob import glob
from threading import Thread

from serial import Serial, SerialException

from pyquest.serial.message import FromDevice, ToDevice

logging.basicConfig(
    filename=__file__.replace(".py", ".log"),
    level=logging.INFO,
    format="%(asctime)s [%(name)s.%(funcName)s] %(levelname)s: %(message)s",
    datefmt="%m/%d/%Y %I:%M:%S %p",
    filemode="a",
)

RESET_COMMAND = b"\x15"


class QuestSerial(Thread):
    def __init__(self, task_queue, data_queue, devices, baudrate=38400, win=False):
        self.logger = logging.getLogger('Serial')
        self.logger.setLevel(logging.DEBUG)
        self.handler = logging.FileHandler(
            __file__.replace(".py", ".log"), "a", "utf-8"
        )
        self.formatter = logging.Formatter(
            fmt="%(asctime)s [%(name)s.%(funcName)s] %(levelname)s: %(message)s",
            datefmt="%m/%d/%Y %I:%M:%S %p",
        )
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler = self.handler

        self.timeout = 1
        self.write_timeout = 1
        if win:
            port = "COM"
            self.port_num = 2  # COM1 как правило системный и бесполезен
            self.maxport_num = 40
            self.port = port
            self.ser = Serial()
            self.ser.timeout = 0.5
            self.ser.write_timeout = 0.5
            self.ser.baudrate = baudrate
            while not self.ser.is_open:
                try:
                    self.ser.port = self.port + str(self.port_num)
                    self.ser.open()
                    time.sleep(0.05)
                    self.ser.reset_input_buffer()
                    self.ser.reset_output_buffer()
                except SerialException:
                    self.logger.debug("Failed serial connection to %s", self.ser.port)
                    if self.port_num < self.maxport_num:
                        self.port_num += 1
                        time.sleep(1)
                    else:
                        break
        else:
            port = glob("/dev/ttyUSB*")
            if not port:
                self.port = "/dev/null"
            else:
                port = port[0]
                self.port = port
            self.ser = Serial()
            self.ser.timeout = 0.5
            self.ser.write_timeout = 0.5
            self.ser.baudrate = baudrate
            self.ser.port = self.port
            while not self.ser.is_open:
                try:
                    self.ser.open()
                    time.sleep(0.05)
                    self.ser.reset_input_buffer()
                    self.ser.reset_output_buffer()
                except SerialException:
                    self.logger.debug("Failed serial connection to %s", self.ser.port)
                    time.sleep(1)
        self.write_tries_limit = 2
        self.current_try = 1
        # self.try_once = ToDevice(RESET_COMMAND, RESET_COMMAND)
        Thread.__init__(self)
        self.task_queue = task_queue
        self.data_queue = data_queue

    def run(self):
        self.logger.debug("start serial thread")
        while True:
            try:
                if not self.ser.is_open:
                    self.ser.open()
                    self.ser.reset_input_buffer()
                    self.ser.reset_output_buffer()
                task = self.task_queue.get()
                self.logger.debug("got something in my queue: %s", task.pack())
                while True:
                    if self.current_try > self.write_tries_limit:
                        self.logger.debug("write failed")
                        self.current_try = 1
                        break
                    self.logger.debug("try: %s", task.pack())
                    result = self.write(task)
                    if result:  # or task.pack() == self.try_once.pack():
                        self.current_try = 1
                        break
                    self.current_try += 1
                    # continue
            except SerialException:
                self.logger("Lost serial connection")
                self.current_try = 1
                time.sleep(1)
                # continue

    def join(self, timeout=None):
        # self.logger.debug('killing serial thread')
        self.ser.close()

    def write(self, task):
        self.logger.debug("Writing to serial")
        try:
            self.ser.write(task.pack())
        except SerialException:
            self.logger.debug("Write exception")

        return self.readLine(task)

    def readLineSimple(self):
        """get line from arduino software serial println(), without header"""
        self.logger.debug("reading response string")
        reads = 0
        msg = ""
        while True:
            if reads > 25:
                self.logger.debug("failed to read response: too long")
                return False
            if not self.ser.in_waiting:
                # self.logger.debug('nothing waiting')
                print("No data")
                time.sleep(.001)
                reads += 1
            else:
                raw = list(self.ser.read(1))
                if not raw:
                    # self.logger.debug('nothing read')
                    print("Nothing read")
                    reads += 1
                    continue
                symbol = raw[0]
                if symbol not in (0, 1):
                    reads += 1
                    msg += str(chr(symbol))
                    # try:
                    #    self.logger.debug('Read: '+msg)
                    print("Char: %s" % symbol)
                    print("Read: " + msg)
                    # except:
                    #     self.logger.debug('Read Error')
                else:
                    if symbol == 13:
                        continue  # because CRLF
                    # params = '/response?device=test&reply='+msg #+'&command='+str(task.pack())
                    # self.logger.debug('params: '+params)
                    # urllib.request.urlopen('http://127.0.0.1'+params)
                    return True

    def readLine(self, task):
        """read 2-char header 0x03, 0x03"""
        self.logger.debug("reading response")
        reads = 0
        debug_str = ""
        while True:
            # self.logger.debug('read attempts: %d', reads)
            if reads > 25:
                self.logger.debug('read: %s', debug_str)
                self.logger.debug('reads count: %s', reads)
                self.logger.debug('failed to read response: too long')
                return False
            if not self.ser.in_waiting:
                # self.logger.debug('nothing waiting')
                debug_str += ' '
                time.sleep(.003)
                reads += 1
            else:
                raw = list(self.ser.read(1))
                if not raw:
                    self.logger.debug("ERR: got smth, but nothing read")
                    reads += 1
                    continue
                symbol = raw[0]
                # self.logger.debug('first symbol: %s', str(symbol))
                # print('first symbol: ' + str(symbol) + ' '+str(chr(symbol)))
                debug_str += str(symbol)
                if symbol != 3:
                    reads += 1
                    continue
                raw = list(self.ser.read(1))
                if not raw:
                    reads += 1
                    continue
                symbol = raw[0]
                debug_str += str(symbol)
                # self.logger.debug('second symbol: %s', str(symbol))
                # print('second symbol: ' + str(symbol)+ ' '+str(chr(symbol)))
                if symbol != 3:
                    reads += 1
                    continue

                self.logger.debug('reads count: %s', reads)
                self.logger.debug('read: %s', debug_str)
                return self.readMessage(task)

    def readMessage(self, task):
        """read payload (12) + crc (1)"""
        self.logger.debug("Trying to read message... ")
        raw = self.ser.read(13)
        if not raw:
            self.logger.debug("... no message")
        # print("1message: " + codecs.encode(str(raw), "utf-8"))
        print("1message: " + str(raw))
        self.logger.debug("message: %s", str(raw))
        try:
            # self.logger.debug("message: " + raw.decode("utf-8"))
            # print("message: " + raw.decode("utf-8"))
            res = FromDevice(raw)
        except ValueError:
            self.logger.debug("CRC Error")
            return False
        if res.address != task.address:
            print("error: different address!")
            self.logger.debug("error: different address!")
            return False
        self.logger.debug("success")
        self.data_queue.put(
            {"device": res.address, "command": task.command, "reply": res.reply}
        )
        return True
