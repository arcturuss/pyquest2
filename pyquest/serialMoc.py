from string import ascii_letters
from random import choice


def data(n):
    return "".join(choice(ascii_letters) for _ in range(n))


class Serial:

    def __init__(self):
        self.timeout = 0
        self.port = "sheat"
        self.baudrate = 0
        self.in_waiting = 5

    def is_open(self):
        return True

    def open(self):
        return True

    def write(self, input):
        return True

    def read(self, n=1):
        return data(n).encode()

    def close(self):
        return True

    def reset_input_buffer(self):
        return True

    def reset_output_buffer(self):
        return True
