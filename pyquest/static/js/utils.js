// var host = "http://"+window.location.hostname;
var host = location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');

$(document).ready(function () {
    getupdate();
    setInterval('getupdate()', 2500);
    setInterval('getaudioupdate()', 500);
    updateSoundboard()
    getSoundboardForm()
    var btn = $("#audioSubmit")
    btn.click(event => audioSubmitHandler(event))
});

function updateDevices(data) {
    $.each(data, function (key, val) {
        $("#" + val.htmlid).attr("class", val.htmlclass);
        $("#" + val.htmlid).html(val.status);
        if (val.htmldata !== undefined) {
            $.each(val.htmldata, function (k, v) {
                // костыль
                if ((k == "tools-p0") || (k == "tools-p1") | (k == "tools-p2") | (k == "tools-p3")) {
                    $("#" + k).html(v);    
                }
                else {
                    $("#" + k).attr("class", v);
                }
            });
        }
        // console.log(key+"2: #"+val.htmlid+" class="+val.htmlclass);
    });
}

function getupdate() {
    $.getJSON(host + '/serial/getUpdate', '', updateDevices);
}

function getaudioupdate() {

    $.ajax({
        method: "get",
        url: "/sound/player/notPlaying/list",
        success: players => {
            for (var n in players) {
                console.log(n + ", "+players[n]);
                $("#audio-" + n + " p.item").each(function(i) {
                    if ($(this).attr("id") == "audio-" + n + "-" + players[n]) {
                        $(this).addClass("playing");
                        $(this).children("#play").removeClass("btn-light").addClass("btn-success")
                        $(this).children("#play_repeat").removeClass("btn-light").addClass("btn-success")
                    }
                    else {
                        $(this).removeClass("playing");
                        $(this).children("#play").removeClass("btn-success").addClass("btn-light");
                        $(this).children("#play_repeat").removeClass("btn-success").addClass("btn-light");
                    }
                });
            }
        }
    });    
    $.ajax({
        method: "get",
        url: "/sound/player/notPlaying/simple",
        success: names => {
            for (var i in names) {
                // console.log(i + ", "+names[i]);
                $("#audiofile-" + names[i] + " #play").removeClass("btn-success").addClass("btn-light")
                $("#audiofile-" + names[i] + " #play_repeat").removeClass("btn-success").addClass("btn-light")
                $("#audiofile-" + names[i]).removeClass("playing")
            }
        }
    });
}

function updateSoundboard() {
    var href = "/sound/table"
    $("#soundboard_content").load(href, () => {
	var players = $("[id^=player-]")
	for (index in players) {
	    players[index].click(event => addFileToPlayerHandler(event))
	}
    })
}

function getSoundboardForm() {
    var href = "/sound/form"
    $("#soundboard_form").load(href, () => { 
	var addFileBtn = $("#audioSubmit")
	addFileBtn.click(event => audioSubmitHandler(event))
	var createPlayerBtn = $("#playerCreate")
	createPlayerBtn.click(event => playerCreateHandler(event))
    })
}

function audioDeleteHandler(index) {
    $.ajax({
        method: "delete",
        url: "/sound/player/" + index,
        success: () => {
            updateSoundboard()
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}

function volumeDownHandler(index) {
    $.ajax({
        method: "get",
        url: "/sound/player/" + index + "/volume_down",
        success: () => {
            updateVolume(index)
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}

function volumeUpHandler(index) {
    $.ajax({
        method: "get",
        url: "/sound/player/" + index + "/volume_up",
        success: () => {
            updateVolume(index)
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}

function bgVolumeDownHandler(name, index) {
    $.ajax({
        method: "get",
        url: "/sound/player/" + name + "/" + index + "/volume_down",
        success: () => {
            bgUpdateVolume(name, index)
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}

function bgVolumeUpHandler(name, index) {
    $.ajax({
        method: "get",
        url: "/sound/player/" + name + "/" + index + "/volume_up",
        success: () => {
            bgUpdateVolume(name, index)
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}

function bgUpdateVolume(name, index) {
    $.ajax({
        method: "get",
        url: "/sound/player/" + name + "/" + index + "/volume",
        success: (volume) => {
            $("#volume-" + name + "-" + index).text(volume)
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}

function muteHandler(index) {
    $.ajax({
        method: "get",
        url: "/sound/player/" + index + "/mute",
        success: () => {
            var btn = $("#mute-" + index)
            var classList = btn.attr("class").split(/\s+/);
            $.each(classList, (index, item) => {
                if (item === "btn-light") {
                    btn.removeClass("btn-light").addClass("btn-danger")
                    return
                }
                if (item === "btn-danger") {
                    btn.removeClass("btn-danger").addClass("btn-light")
                    return
                }
            });
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}

function updateVolume(index) {
    $.ajax({
        method: "get",
        url: "/sound/player/" + index + "/volume",
        success: (volume) => {
            $("#volume-" + index).text(volume)
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}

function playHandler(index) {
    $.ajax({
        method: "get",
        url: "/sound/player/" + index + "/play",
        success: (volume) => {
            updateVolume(index)
            $("#audiofile-" + index + " #play").removeClass("btn-light").addClass("btn-success")
            $("#audiofile-" + index + " #play_repeat").removeClass("btn-success").addClass("btn-light")
            $("#audiofile-" + index).addClass("playing")
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}

function playRepeatHandler(index) {
    $.ajax({
        method: "get",
        url: "/sound/player/" + index + "/play_and_repeat",
        success: (volume) => {
            updateVolume(index)
            $("#audiofile-" + index + " #play_repeat").removeClass("btn-light").addClass("btn-success")
            $("#audiofile-" + index + " #play").removeClass("btn-success").addClass("btn-light")
            $("#audiofile-" + index).addClass("playing")
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}

function stopHandler(index) {
    $.ajax({
        method: "get",
        url: "/sound/player/" + index + "/stop",
        success: (volume) => {
            $("#audiofile-" + index + " #play").removeClass("btn-success").addClass("btn-light")
            $("#audiofile-" + index + " #play_repeat").removeClass("btn-success").addClass("btn-light")
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}

function audioSubmitHandler(event) {
    event.preventDefault()

    var form = $("form#audioUpload")[0]
    var data = new FormData(form)
    console.log(form)
    $("#audioSubmit").prop("disabled", true)
    var request = {
        type: "post",
        url: "/sound/file",
        data: data,
        processData: false,
        contentType: false,
        timeout: 600000,
        success: () => {
            getSoundboardForm()
            $("#audioSubmit").prop("disabled", false)
        },
        error: e => {
            console.log("ОШИБКА: ", e)
            $("#audioSubmit").prop("disabled", false)
        }
    };

    $.ajax(request)
}

function playerCreateHandler(event) {
    event.preventDefault()

    var form = $("form#audioUpload")
    $("#playerCreate").prop("disabled", true)
    var request = {
	type: "post",
	url: "/sound/player",
	data: form.serialize(),
	success: () => {
	    updateSoundboard()
	    $("#playerCreate").prop("disabled", false)
	},
	error: e => {
	    console.log("ОШИБКА: ", e)
            $("#playerCreate").prop("disabled", false)
	}
    }

    $.ajax(request)
}

function addFileToPlayerHandler(event) {
    event.preventDefault()
    console.log(event)
}

function bgPlayHandler(name, index) {
    $.ajax({
        method: "get",
        url: "/sound/player/" + name + "/" + index + "/play",
        success: (volume) => {
            updateVolume(name)
            $("#audio-" + name + "-" + index + " #play").removeClass("btn-light").addClass("btn-success")
            $("#audio-" + name + "-" + index + " #play_repeat").removeClass("btn-success").addClass("btn-light")
            $("#audio-" + name + "-" + index).addClass("playing")
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}

function testCall() {
    $.ajax({
        method: "get",
        url: "/sound/player/testcall",
        success: () => {
        },
        error: e => {
            console.log("ОШИБКА: ", e)
        }
    })
}
