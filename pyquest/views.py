#!/usr/bin/python
import logging
import sys

from flask import render_template

from config import SOUND_DEVICES
from pyquest import app

# sys.setdefaultencoding() does not exist, here!
# reload(sys)  # Reload does the trick!
# sys.setdefaultencoding('UTF8')
print(sys.getdefaultencoding())

logger = logging.getLogger("server loop")
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(__file__.replace(".py", ".log"), "a", "utf-8")
formatter = logging.Formatter(
    fmt="%(asctime)s [%(name)s.%(funcName)s] %(levelname)s: %(message)s",
    datefmt="%m/%d/%Y %I:%M:%S %p",
)
handler.setFormatter(formatter)
logger.addHandler = handler
logger.debug("init")


@app.route("/update")
def postUpdate():
    pass
    return ("", 200)


@app.route("/")
def hello_world():
    return render_template("index.html")
