from collections import OrderedDict
from pathlib import Path

# AUDIO_ROOT = Path("/home/infernal/Music/jail")
#audio_path = "c:/Users/Arcturus/YandexDisk/quest/PTEST3/pyquest/pyquest_sound"
audio_path = "/home/infernal/Music/jail"
AUDIO_ROOT = Path(audio_path)
SOUND_DEVICES = OrderedDict(
    {
        "room_stereo": b"room",
        "room_left": b"room-left",
        "room_right": b"room-right",
        "shed_stereo": b"shed",
        "shed_left": b"shed-left",
        "shed_right": b"shed-right",
        "train": b"train",
        "jail": b"jail",
    }
)

SPECIAL_FILES = {
    "train_sound": SOUND_DEVICES["train"],
    "arrow_sound": SOUND_DEVICES["room_stereo"],
}

DEFAULT_VOLUME = 80
PLAYER_CONF_FILE = Path(audio_path+"/player_config.json")
