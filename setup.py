try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'Quest server',
    'author': 'infernal',
    'url': 'nope',
    'download_url': 'nope',
    'author_email': 'nope',
    'version': '0.1',
    'install_requires': ['nose', 'flask', 'pyserial'],
    'packages': ['pyquest'],
    'scripts': [],
    'name': 'pyquest'
}

setup(**config)
